#!/usr/bin/bash
# Проверка режима загрузки
if ls /sys/firmware/efi/efivars
then
echo 'Your boot mode is EFI'
# Сихронизация системных часов
timedatectl set-ntp true
# Создание разделов
echo -e ',50668M,L\n,+,U\n' | sfdisk -X gpt -w always --lock /dev/sda
# Форматирование разделов
mkfs.ext4 /dev/sda1
mkfs.fat -F 32 /dev/sda2
# Монтирование разделов
mount /dev/sda1 /mnt
mount -m /dev/sda2 /mnt/boot
# Установка необходимых пакетов
pacstrap /mnt base linux-lts linux-firmware intel-ucode sudo nano
# Генерация fstab
genfstab -U /mnt >> /mnt/etc/fstab
# Вход в chroot
arch-chroot /mnt
# Задание часового пояса
ln -sf /usr/share/zoneinfo/Asia/Omsk /etc/localtime
hwclock --systohc
# Локализация
sed -ie 's/#en_US.UTF-8/en_US.UTF-8/; s/#ru_RU.UTF-8/ru_RU.UTF-8/' /etc/locale.gen
locale-gen
echo 'LANG=ru_RU.UTF-8' > /etc/locale.conf
echo -e 'KEYMAP=ru\nFONT=cyr-sun16' > /etc/vconsole.conf
# Конфигурация сети
echo -n 'Enter hostname: '
read HOSTNAME
echo '$HOSTNAME' > /etc/hostname
echo -e '127.0.0.1\tlocalhost\n::1      \tlocalhost\n127.0.1.1\t$HOSTNAME' >> /etc/hosts
# Регенерация initramfs
mkinitcpio -P
# Задание пароля для суперпользователя
passwd
# Установка загрузчика (systemd-boot)
bootctl install
echo -e 'default arch.conf\nconsole-mode max\neditor   no' > /boot/loader/loader.conf
echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux-lts\ninitrd  /intel-ucode.img\ninitrd  /initramfs-linux-lts.img\noptions root=UUID=`lsblk -dno UUID /dev/sda1` rw' > /boot/loader/entries/arch.conf
echo -e 'title   Arch Linux (fallback initramfs)\nlinux   /vmlinuz-linux-lts.img\ninitrd  /intel-ucode.img\ninitrd  /initramfs-linux-lts-fallback.img\noptions root=UUID=`lsblk -dno UUID /dev/sda1` rw' > /boot/loader/entries/arch-fallback.conf
# Выход из chroot
exit
# Размонтирование разделов
umount -R /mnt
# Выключение системы
systemctl poweroff
else
echo 'Your boot mode is Legacy'
fi
